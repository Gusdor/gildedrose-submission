﻿using GildedRose.Console.Inventory;
using System.Collections.Generic;
using System.Linq;

namespace GildedRose.Console
{
    class Program
    {
        public IList<Item> Items;
        private readonly IItemCategory defaultCategory;

        static void Main(string[] args)
        {
            System.Console.WriteLine("OMGHAI!");

            // Initialise items
            var inventory = new InventoryStore();

            inventory.Add(
                category: new LegendaryItem(),
                item: new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80 });

            inventory.Add(
                category: new VintageItem(),
                item: new Item { Name = "Aged Brie", SellIn = 2, Quality = 0 });

            inventory.Add(
                category: new BackStagePass(),
                item: new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 15,
                    Quality = 20
                });

            inventory.Add(
                new Item { Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20 },
                new Item { Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7 },
                new Item { Name = "Conjured Mana Cake", SellIn = 3, Quality = 6 });

            var app = new Program(new StandardItem())
            {
                Items = inventory.GetItems()
            };

            app.UpdateQuality(inventory);

            System.Console.ReadKey();

        }

        public Program(IItemCategory defaultCategory)
        {
            if (defaultCategory == null)
            {
                throw new System.ArgumentNullException(nameof(defaultCategory));
            }

            this.defaultCategory = defaultCategory;
        }

        public void UpdateQuality(InventoryStore inventory)
        {
            if (inventory == null)
            {
                throw new System.ArgumentNullException(nameof(inventory));
            }

            foreach (var item in this.Items)
            {
                var itemBehaviour = inventory[item] ?? this.defaultCategory;

                itemBehaviour.Update(item);
            }

            return;
        }

    }

    public class Item
    {
        public string Name { get; set; }

        public int SellIn { get; set; }

        public int Quality { get; set; }
    }

}
