﻿namespace GildedRose.Console.Inventory
{
    interface IItemCategory
    {
        void Update(Item item);
    }
}
