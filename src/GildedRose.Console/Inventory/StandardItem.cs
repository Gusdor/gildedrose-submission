﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console.Inventory
{
    /// <summary>
    /// The category for standard items, where the quality and 
    /// sell-in count decrease by 1 for each daily update.
    /// </summary>
    class StandardItem: IItemCategory
    {
        protected const int STANDARD_QUALITY_CHANGE_RATE = 1;
        const int MIN_SELLIN = 0;
        const int MAX_QUALITY = 50;
        const int MIN_QUALITY = 0;

        static int EnforceBoundaries(int value, int min, int max)
        {
            return Math.Max(min, Math.Min(max, value));
        }

        protected void AmendQuality(Item item, int amount)
        {
            item.Quality = EnforceBoundaries(item.Quality + amount, MIN_QUALITY, MAX_QUALITY);
        }

        protected void AmendSellIn(Item item, int amount)
        {
            item.SellIn = EnforceBoundaries(item.SellIn + amount, MIN_SELLIN, int.MaxValue);
        }

        public virtual void Update(Item item)
        {
            AmendQuality(item, -STANDARD_QUALITY_CHANGE_RATE);
            AmendSellIn(item, -STANDARD_QUALITY_CHANGE_RATE);
        }
    }
}
