﻿using GildedRose.Console.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console.Inventory
{
    class InventoryStore
    {
        private readonly IDictionary<Item, IItemCategory> categoryMap =
            new Dictionary<Item, IItemCategory>();

        /// <summary>
        /// Adds a new item to the inventory.
        /// </summary>
        /// <param name="item">The item to add</param>
        /// <param name="category">Alternative category for the item. If null, the default category is assumed.</param>
        public void Add(Item item, IItemCategory category = null)
        {
            this.categoryMap.Add(item, category);
        }

        public void Add(IEnumerable<Item> items, IItemCategory category = null)
        {
            foreach (var item in items)
            {
                this.categoryMap.Add(item, category);
            }
        }

        public void Add(params Item[] items)
        {
            this.Add(items, null);
        }

        public List<Item> GetItems()
        {
            return this.categoryMap.Keys.ToList();
        }

        public IItemCategory this[Item item]
        {
            get { return this.categoryMap[item]; }
        }
    }
}
