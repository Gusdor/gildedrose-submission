﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console.Inventory
{
    /// <summary>
    /// A category for items whose quality decreases at twice the normal rate.
    /// </summary>
    class ConjuredItem : StandardItem
    {
        public override void Update(Item item)
        {
            base.AmendQuality(item, 2 * -STANDARD_QUALITY_CHANGE_RATE);
            base.AmendSellIn(item, -STANDARD_QUALITY_CHANGE_RATE);
        }
    }
}
