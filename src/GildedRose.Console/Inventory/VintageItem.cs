﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console.Inventory
{
    /// <summary>
    /// A category for items whose quality increases as they age.
    /// </summary>
    class VintageItem : StandardItem
    {
        public override void Update(Item item)
        {
            base.AmendQuality(item, STANDARD_QUALITY_CHANGE_RATE);
            base.AmendSellIn(item, -STANDARD_QUALITY_CHANGE_RATE);
        }
    }
}
