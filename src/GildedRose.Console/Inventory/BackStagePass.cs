﻿namespace GildedRose.Console.Inventory
{
    class BackStagePass : StandardItem
    {
        public override void Update(Item item)
        {
            base.AmendSellIn(item, -1);

            this.UpdateQuality(item);
        }

        private void UpdateQuality(Item item)
        {
            int amendBy;
            if (item.SellIn <= 0)
            {
                amendBy = -item.Quality;
            }
            else if (item.SellIn <= 5)
            {
                amendBy = 3;
            }
            else if (item.SellIn <= 10)
            {
                amendBy = 2;
            }
            else
            {
                amendBy = 1;
            }

            base.AmendQuality(item, amendBy);
        }
    }
}
