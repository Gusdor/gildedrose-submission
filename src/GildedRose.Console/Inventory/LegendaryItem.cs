﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console.Inventory
{
    /// <summary>
    /// An item category where the sell-in count and quality
    /// never change.
    /// </summary>
    class LegendaryItem : IItemCategory
    {
        public void Update(Item item)
        {
            // Do nothing
        }
    }
}
