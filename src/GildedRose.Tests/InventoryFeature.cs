using GildedRose.Console;
using GildedRose.Console.Inventory;
using System.Collections.Generic;
using Xunit;

namespace GildedRose.Tests
{
    public class InventoryFeature
    {
        [Fact]
        public void QualityDegradesWhenUpdated()
        {
            GivenAnItem(quality: 10, sellIn: 10);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 9, sellIn: 9);
        }

        [Fact]
        public void QualityIsNeverNegativeWhenUpdated()
        {
            GivenAnItem(quality: 0, sellIn: 2);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 0, sellIn: 1);
        }

        [Fact]
        public void AgedBrieGetsBetterWithAge()
        {
            GivenAVintageItem(name: "Aged Brie", quality: 0, sellIn: 2);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 1, sellIn: 1);
        }

        [Fact]
        public void QualityCanNeverIncreaseToOver50()
        {
            GivenAVintageItem(name: "Aged Brie", quality: 50, sellIn: 2);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 50, sellIn: 1);
        }

        [Fact]
        public void QualityOfSulfurasNeverDecreases()
        {
            GivenALegendaryItem(name: "Sulfuras, Hand of Ragnaros", quality: 80);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 80);
        }

        [Fact]
        public void SulfurasNeverNeedsToBeSold()
        {
            GivenALegendaryItem(name: "Sulfuras, Hand of Ragnaros", sellIn: 10);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(sellIn: 10);
        }

        [Fact]
        public void BackstagePassGetsBetterWithAge()
        {
            GivenABackstagePass(name: "Backstage passes to a TAFKAL80ETC concert", quality: 0, sellIn: 20);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 1, sellIn: 19);
        }

        [Fact]
        public void BackstagePassQualityIsZeroAfterConcert()
        {
            GivenABackstagePass(name: "Backstage passes to a TAFKAL80ETC concert", quality: 10, sellIn: 0);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 0, sellIn: 0);
        }

        [Fact]
        public void BackstagePassQualityIncreasesFasterIfConcertIsUnderTenOrLessDaysAway()
        {
            GivenABackstagePass(name: "Backstage passes to a TAFKAL80ETC concert", quality: 0, sellIn: 10);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 2, sellIn: 9);
        }

        [Fact]
        public void BackstagePassQualityIncreasesVeryIfConcertIsUnderFiveOrLessDaysAway()
        {
            GivenABackstagePass(name: "Backstage passes to a TAFKAL80ETC concert", quality: 0, sellIn: 5);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 3, sellIn: 4);
        }

        [Fact]
        public void ConjuredItemsDegradeTwiceAsFast()
        {
            GivenAConjuredItem(quality: 10, sellIn: 10);

            WhenDailyUpdateOccurs();

            ThenTheItemWillHave(quality: 8, sellIn: 9);
        }

        class ItemContext
        {
            public Item Item { get; set; }
            public IItemCategory Category { get; set; }
        }

        private ItemContext context;

        private void GivenALegendaryItem(int quality = 10, int sellIn = 10, string name = null)
        {
            this.context = new ItemContext()
            {
                Item = new Item() { Name = name, Quality = quality, SellIn = sellIn },
                Category = new LegendaryItem()
            };
        }

        private void GivenAVintageItem(int quality = 10, int sellIn = 10, string name = null)
        {
            this.context = new ItemContext()
            {
                Item = new Item() { Name = name, Quality = quality, SellIn = sellIn },
                Category = new VintageItem()
            };
        }

        private void GivenABackstagePass(int quality = 10, int sellIn = 10, string name = null)
        {
            this.context = new ItemContext()
            {
                Item = new Item() { Name = name, Quality = quality, SellIn = sellIn },
                Category = new BackStagePass()
            };
        }

        private void GivenAConjuredItem(int quality = 10, int sellIn = 10, string name = null)
        {
            this.context = new ItemContext()
            {
                Item = new Item() { Name = name, Quality = quality, SellIn = sellIn },
                Category = new ConjuredItem()
            };
        }

        private void GivenAnItem(int quality = 10, int sellIn = 10, string name = null)
        {
            this.context = new ItemContext()
            {
                Item = new Item() { Name = name, Quality = quality, SellIn = sellIn },
                Category = null
            };
        }

        private void WhenDailyUpdateOccurs()
        {
            var program = new Program(new StandardItem()) { Items = new List<Item> { context.Item } };
            var inventory = new InventoryStore();
            inventory.Add(context.Item, context.Category);
            program.UpdateQuality(inventory);
        }

        void TestIfHasValue<T>(T? expected, T actual) where T : struct
        {
            if (expected.HasValue)
            {
                Assert.Equal(expected.Value, actual);
            }
        }

        private void ThenTheItemWillHave(int? quality = null, int? sellIn = null)
        {
            TestIfHasValue(quality, context.Item.Quality);
            TestIfHasValue(sellIn, context.Item.SellIn);
        }
    }
}